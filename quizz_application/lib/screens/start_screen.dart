import 'package:flutter/material.dart';
import 'package:quizz_application/screens/home_screen.dart';
import 'package:quizz_application/screens/quiz_screen.dart';

class LandingScreenDesign extends StatefulWidget {
  const LandingScreenDesign({super.key});

  @override
  State<LandingScreenDesign> createState() {
    return _LandingScreenDesignState();
  }
}

class _LandingScreenDesignState extends State<LandingScreenDesign> {
  Widget? activeScreen;

  @override
  void initState() {
    activeScreen = HomeScreenDesign(switchScreen);
    super.initState();
  }

  void switchScreen() {
    setState(() {
      activeScreen = const QuizScreenDesign();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.white,
            Color.fromARGB(255, 234, 216, 255),
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      child: activeScreen,
    );
  }
}
