import 'package:flutter/material.dart';
import 'package:quizz_application/data/dummy_datas.dart';

class QuizScreenDesign extends StatefulWidget {
  const QuizScreenDesign({super.key});

  @override
  State<QuizScreenDesign> createState() {
    return _QuizScreenDesignState();
  }
}

class _QuizScreenDesignState extends State<QuizScreenDesign> {
  @override
  Widget build(BuildContext context) {
    final currentQuestion = questions[0];
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            currentQuestion.question,
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: () {},
            child: Text(
              currentQuestion.answers[0],
            ),
          ),
          ElevatedButton(
            onPressed: () {},
            child: Text(
              currentQuestion.answers[1],
            ),
          ),
          ElevatedButton(
            onPressed: () {},
            child: Text(
              currentQuestion.answers[2],
            ),
          ),
        ],
      ),
    );
  }
}
