import 'package:flutter/material.dart';
import 'package:quizz_application/screens/app_bar.dart';
import 'package:quizz_application/screens/start_screen.dart';

class PageConstructor extends StatelessWidget {
  const PageConstructor({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        appBar: AppBarDesign(),
        body: LandingScreenDesign(),
      ),
    );
  }
}
