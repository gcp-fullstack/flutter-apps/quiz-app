import 'package:flutter/material.dart';

class HomeScreenDesign extends StatelessWidget {
  const HomeScreenDesign(this.startQuiz, {super.key});

  final void Function() startQuiz;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Image.asset(
            "assets/images/bg.png",
            color: const Color.fromARGB(255, 255, 191, 0),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "Let Learn Flutter From Fun Way!!!",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          OutlinedButton.icon(
            onPressed: startQuiz,
            style: OutlinedButton.styleFrom(
              foregroundColor: Colors.black,
              backgroundColor: const Color.fromARGB(255, 255, 191, 0),
            ),
            icon: const Icon(Icons.arrow_circle_right_outlined),
            label: const Text(
              "Start",
              style: TextStyle(color: Colors.black),
            ),
          ),
        ],
      ),
    );
  }
}
