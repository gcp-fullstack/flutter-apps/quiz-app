import 'package:flutter/material.dart';

class AppBarDesign extends StatelessWidget implements PreferredSizeWidget {
  const AppBarDesign({super.key});

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: const Color.fromARGB(255, 255, 212, 95),
      title: const Text(
        "Quiz Application!",
        style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
      ),
      centerTitle: true,
    );
  }
}
